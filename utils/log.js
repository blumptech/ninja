const chalk = require("chalk");

const log = (data) => {
  let result = "";
  if (data.stdout) {
    result += data.stdout;
  }
  if (data.stderr) {
    result += data.stderr;
  }
  console.log(chalk.white(result));
};

module.exports = log;
