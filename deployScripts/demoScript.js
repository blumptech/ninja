const fs = require("fs");
const path = require("path");
const chalk = require("chalk");
const { NodeSSH } = require("node-ssh");
const { CNS_IP, CNS_USERNAME } = require("../config");
const ssh = new NodeSSH();
const log = require("./../utils/log");

module.exports.demoDeployer = async () => {
  try {
    await ssh.connect({
      host: CNS_IP,
      username: CNS_USERNAME,
      privateKey: fs.readFileSync(
        path.join(__dirname, "/../SSHKeys/cns.key"),
        "utf8"
      ),
    });

    console.log(chalk.blue("RUNNING GIT PULL"));
    result = await ssh.execCommand("git pull", {
      cwd: "/home/ubuntu/test123",
    });
    log(result);

    console.log(chalk.blue("RUNNING NPM I"));
    result = await ssh.execCommand("npm i ", {
      cwd: "/home/ubuntu/test123",
    });
    log(result);

    // console.log(chalk.blue("REINSTALLING PM2"))
    // result = await ssh.execCommand("npm i pm2 -g", {
    //   cwd: "/home/ubuntu/test123",
    // });
    // console.log(result);
    //...................
    console.log(chalk.blue("MAKING BUILD"));
    result = await ssh.execCommand("npm run build", {
      cwd: "/home/ubuntu/test123",
    });
    log(result);
    //............
    //killing an app
    // let result = await ssh.execCommand("ps -e|grep node");
    // console.log(result);

    // if (result.stdout) {
    //   let processid = parseInt(result.stdout);
    //   console.log("previoulsy node app running");
    //   console.log("found on ", processid);
    //   console.log("killing node app");
    //   result = await ssh.execCommand("kill " + processid);
    // } else {
    //   console.log(" no previoulsy  running node app");
    // }
    //.........
    console.log(chalk.blue("STOPPING APP"));
    result = await ssh.execCommand(
      "/home/ubuntu/.nvm/versions/node/v14.17.6/bin/forever stopall",
      {
        cwd: "/home/ubuntu/test123",
      }
    );
    log(result);

    // result = await ssh.execCommand(
    //   "/home/ubuntu/.nvm/versions/node/v14.17.6/bin/pm2 delete all",
    //   {
    //     cwd: "/home/ubuntu/test123",
    //   }
    // );
    // console.log(result);

    console.log(chalk.blue("RESTARTING APP"));
    result = await ssh.execCommand(
      "/home/ubuntu/.nvm/versions/node/v14.17.6/bin/forever start server/app.js",
      {
        cwd: "/home/ubuntu/test123",
      }
    );
    log(result);

    console.log(chalk.bgGreen("DEPLOYEMNT COMPLETED! "));
    console.log(chalk.magenta("GO AND SLEEP NOW! "));

    ssh.dispose();
  } catch (e) {
    console.log(e);
  }
};
